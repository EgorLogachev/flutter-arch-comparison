import 'package:flutter/material.dart';
import 'package:flutter_arch_samples/bloclib/counter.dart';
import 'package:flutter_arch_samples/bloclib/home_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class BlocLibApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (_) => CounterBloc(),
        child: BlocListener<CounterBloc, int>(
          listener: (context, state) {
            if ((state % 10) == 0) {
              showDialog(
                context: context,
                builder: (_) => AlertDialog(
                  title: Text('Congratulations! You clicked $state times'),
                ),
              );
            }
          },
          child: HomeScreen(title: 'Bloc Library Demo Home Page'),
        ),
      ),
    );
  }
}