import 'package:flutter/material.dart';
import 'package:flutter_arch_samples/bloc/bloc_app.dart';
import 'package:flutter_arch_samples/bloclib/bloc_lib_app.dart';
import 'package:flutter_arch_samples/provider/provider_app.dart';

void main() => runApp(BlocApp());