import 'package:flutter/material.dart';
import 'package:flutter_arch_samples/bloc/home_screen.dart';


class BlocApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen()
    );
  }
}