import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_arch_samples/bloc/home_layout.dart';
import 'bloc_components.dart';
import 'counter.dart';

/// entity that encapsulate mapping Layout(UI), Bloc(Business Logic), listener(Navigation Handler) on BlocProvider
class HomeScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CounterBloc, int>(
      blocValue: () => CounterBloc(),
      child: HomeLayout(title: 'Bloc Demo Home Page'),
      listener: onNavigation,
    );
  }

  void onNavigation(context, state) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text('Congratulations! You clicked $state times'),
      ),
    );
  }
}
