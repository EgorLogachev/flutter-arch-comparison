
import 'package:mobx/mobx.dart';

part 'counter.g.dart';

class Counter = CounterBase with _$Counter;

/// Business logic layer
abstract class CounterBase with Store {

  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
