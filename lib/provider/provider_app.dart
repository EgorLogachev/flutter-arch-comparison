import 'package:flutter/material.dart';
import 'package:flutter_arch_samples/provider/counter.dart';
import 'package:flutter_arch_samples/provider/home_screen.dart';
import 'package:provider/provider.dart';

class ProviderApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ChangeNotifierProvider(
        create: (_) => Counter(),
        child: HomeScreen(title: 'Provider Demo Home Page'),
      ),
    );
  }
}